extends Node


var player_projectile_basic: PackedScene = load("res://scenes/system/projectiles/p_basic.tscn")
var enemy_projectile_basic: PackedScene = load("res://scenes/system/projectiles/e_basic.tscn")
var enemy_projectile_sphere: PackedScene = load("res://scenes/system/projectiles/e_sphere.tscn")

var player: KinematicBody2D
var main_scene: Node2D

var score: int = 0
var remaining_time: float = 60.0
var player_died: bool = false

var current_round: int = 0


func _ready():
	var asp = AudioStreamPlayer.new()
	add_child(asp)
	asp.stream = load("res://assets/sounds/bgm.ogg")
	asp.volume_db = 0
	asp.play()
	

func get_player_global_position() -> Vector2:
	if player:
		return player.to_global(Vector2(0, 0))
	else:
		return Vector2(0, 0)
