extends Node2D


var active = false
var game_ended = false


func _ready():
	Global.main_scene = self
	Global.score = 0
	Global.remaining_time = 60
	Global.player_died = false

	$Background.play("default")

	if Global.current_round > 0:
		activate()


func _process(delta):
	if Input.is_action_just_pressed("quit_game"):
		get_tree().quit()
	
	if Input.is_action_just_pressed("start_game") and not active and not game_ended:
		activate()
	elif Input.is_action_just_pressed("start_game") and not active and game_ended:
		get_tree().change_scene("res://scenes/main.tscn")
		
	
	if active and Global.remaining_time > 0:
		Global.remaining_time -= delta
		if Global.remaining_time < 0:
			Global.remaining_time = 0
			deactivate()
		elif Global.player_died:
			Global.remaining_time = 0
			deactivate()
			
			
func activate():
	game_ended = false
	active = true
	$Player.active = true
	$EnemySpawnController.active = true
	$OWPlatformBottom.active = true
	$OWPlatformTop.active = true

	Global.current_round += 1
	

func deactivate():
	game_ended = true
	active = false
	$Player.active = false
	$EnemySpawnController.active = false
	$OWPlatformBottom.active = false
	$OWPlatformTop.active = false
