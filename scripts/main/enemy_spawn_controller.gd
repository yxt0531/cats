extends Node2D


var enemy_normal: PackedScene = load("res://scenes/system/enemies/rat_normal.tscn")
var enemy_elite: PackedScene = load("res://scenes/system/enemies/rat_elite.tscn")
var enemy_elite_red: PackedScene = load("res://scenes/system/enemies/rat_elite_red.tscn")


export var spawn_time_interval_min: float = 0.25
export var spawn_time_interval_max: float = 1.5

var spawn_timer: float = 0

var active = false


func _ready():
	randomize()


func _process(delta):
	if not active:
		return
	
	if spawn_timer <= 0:
		var spawn_position_full: bool = true
		for e_spawn_position in get_children():
			if e_spawn_position.get_child_count() == 0: # empty spawn position found
				spawn_position_full = false
				break
				
		if spawn_position_full: # prevent spawning when position is full
			return
		
		var spawn_location: int = randi() % get_child_count()
		while get_child(spawn_location).get_child_count() != 0: # find another empty position if filled
			spawn_location = randi() % get_child_count()
		
		var spawn_probability: int = randi() % 10
		
		var e
		
		if spawn_probability > 8:
			e = enemy_elite.instance()
		elif spawn_probability > 6:
			e = enemy_elite_red.instance()
		else:
			e = enemy_normal.instance()
		
		if spawn_location < 5: # spawn on left
			e.facing_right = true
			
		get_child(spawn_location).add_child(e)
		
		spawn_timer = rand_range(spawn_time_interval_min, spawn_time_interval_max)
	else:
		spawn_timer -= delta

