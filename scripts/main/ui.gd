extends Control


func _process(delta):
	if get_parent().active:
		$FullScreenInfo.hide()
		$LabelScore.show()
		$LabelRemainingTime.show()
		
		$LabelScore.text = "Score: " + str(Global.score)
		$LabelRemainingTime.text = "Time: " + str(int(Global.remaining_time))
	else:
		$FullScreenInfo.show()
		$LabelScore.hide()
		$LabelRemainingTime.hide()
		
		if get_parent().game_ended:
			if Global.player_died:
				$FullScreenInfo/Label.text = "You Died\nScore: " + str(Global.score) + "\n\nPress [Start] to Reset"
			else:
				$FullScreenInfo/Label.text = "Game Over\nScore: " + str(Global.score) + "\n\nPress [Start] to Reset"
	
