extends Area2D


const SPEED: float = 4.0

export var direction: Vector2 = Vector2.LEFT
export var life_time: float = 5.0

export var damage: float = 10.0


func _ready():
	$SpriteOrigin.look_at(to_global(direction))
	
	
func _physics_process(delta):
	if Global.remaining_time <= 0:
		queue_free()
		
	position += direction.normalized() * SPEED

	life_time -= delta
	if life_time < 0:
		queue_free()
		

func _on_VisibilityNotifier_screen_exited():
	queue_free()


func _on_EBasic_body_entered(body):
	if body.collision_layer == 1:
		body.call("damage", damage)
		queue_free()
