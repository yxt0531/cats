extends KinematicBody2D


const SPEED: float = 800.0

export var direction: Vector2 = Vector2.RIGHT
export var life_time: float = 5.0

export var damage: float = 60.0


func _ready():
	$SpriteOrigin.look_at(to_global(direction))


func _physics_process(delta):
	if Global.remaining_time <= 0:
		queue_free()
		
	move_and_slide(direction.normalized() * SPEED)

	life_time -= delta
	if life_time < 0:
		queue_free()
		

func _on_VisibilityNotifier_screen_exited():
	queue_free()
