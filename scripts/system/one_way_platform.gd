extends KinematicBody2D


const DISABLE_DURATION: float = 0.2
const MAX_MOVE_SPEED: float = 120.0
const MOVE_SPEED_ACCEL: float = 240.0

export var min_x_position: float = 120
export var max_x_position: float = 200

export var moving_right: bool = true

var disable_timer: float = 0
var moving_dir: float = 0

onready var original_y: float = position.y

var active: bool = false


func _physics_process(delta):
	if not active:
		return
	
	if disable_timer > 0:
		disable_timer -= delta
	elif disable_timer < 0:
		$CollisionShape.disabled = false
		disable_timer = 0
	
	if position.x > max_x_position:
		moving_right = false
	elif position.x < min_x_position:
		moving_right = true
	
	if moving_right:
		if moving_dir < MAX_MOVE_SPEED:
			moving_dir += MOVE_SPEED_ACCEL * delta
			if moving_dir > MAX_MOVE_SPEED:
				moving_dir = MAX_MOVE_SPEED
	else:
		if moving_dir > -MAX_MOVE_SPEED:
			moving_dir -= MOVE_SPEED_ACCEL * delta
			if moving_dir < -MAX_MOVE_SPEED:
				moving_dir = -MAX_MOVE_SPEED
	
	position.x += moving_dir * delta
	position.y = original_y
	

func disable():
	$CollisionShape.disabled = true
	disable_timer = DISABLE_DURATION
	print(name + " disabled")
