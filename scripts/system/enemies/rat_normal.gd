extends Area2D


const DAMAGE_RED_FLASH_TIME: float = 0.05
const SCORE: int = 20

export var hp: float = 60
export var wait_time: float = 3
export var facing_right: bool = false

var damage_red_flash_timer: float = 0


func _ready():
	$AnimatedSprite.play("default")
	if facing_right:
		$AnimatedSprite.flip_h = true
		

func _process(delta):
	if damage_red_flash_timer > 0:
		damage_red_flash_timer -= delta
		modulate = Color.red
	else:
		modulate = Color.white
		
	if wait_time > 0 and $AnimatedSprite.animation == "wait":
		wait_time -= delta
	elif wait_time < 0 and $AnimatedSprite.animation == "wait":
		wait_time = 0
		$AnimatedSprite.play("retreat")


func _on_RatNormal_body_entered(body):
	if body.get("damage") and hp > 0:
		if not $ASPImpact.playing:
			$ASPImpact.play()
			
		hp -= body.damage
		damage_red_flash_timer = DAMAGE_RED_FLASH_TIME
		
		if hp <= 0:
			$AnimatedSprite.play("die")
			

func _on_AnimatedSprite_animation_finished():
	match $AnimatedSprite.animation:
		"default":
			$AnimatedSprite.play("wait")
		"retreat":
			queue_free()
		"die":
			Global.score += SCORE
			queue_free()
