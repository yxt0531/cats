extends Area2D


const DAMAGE_RED_FLASH_TIME: float = 0.05
const SCORE: int = 30

export var hp: float = 120
export var facing_right: bool = false

var damage_red_flash_timer: float = 0


func _ready():
	$AnimatedSprite.play("default")
	if facing_right:
		$AnimatedSprite.flip_h = true
		$ProjectileSpawnPos.position.x = -$ProjectileSpawnPos.position.x


func _process(delta):
	if damage_red_flash_timer > 0:
		damage_red_flash_timer -= delta
		modulate = Color.red
	else:
		modulate = Color.white


func spawn_projectile():
	if Global.remaining_time <= 0:
		return
		
	var p = Global.enemy_projectile_sphere.instance()
	p.position = to_global($ProjectileSpawnPos.position)
	p.direction = Global.get_player_global_position() - to_global($ProjectileSpawnPos.position)
	
	Global.main_scene.add_child(p)
	

func _on_RatEliteRed_body_entered(body):
	if body.get("damage") and hp > 0:
		if not $ASPImpact.playing:
			$ASPImpact.play()
		
		hp -= body.damage
		damage_red_flash_timer = DAMAGE_RED_FLASH_TIME
		
		if hp <= 0:
			$AnimatedSprite.play("die")
			$AnimationPlayer.stop()


func _on_AnimatedSprite_animation_finished():
	match $AnimatedSprite.animation:
		"default":
			$AnimatedSprite.play("wait")
			$AnimationPlayer.play("fire")
		"retreat":
			queue_free()
		"die":
			Global.score += SCORE
			queue_free()


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "fire":
		$AnimatedSprite.play("retreat")
