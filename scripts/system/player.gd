extends KinematicBody2D


export var hp: float = 100.0

enum Facing {
	LEFT, RIGHT
}

const JUMP_SPEED: float = 350.0
const GRAVITY: float = 700.0
const GRAVITY_CLAMP: float = 4.0
const GRAVITY_PLAYER_MOVING_UP_CO: float = 1.5
const GRAVITY_PLAYER_MOVING_DOWN_CO: float = 1.0

const MOVE_HORIZONTAL_SPEED_MAX: float = 250.0
const MOVE_HORIZONTAL_SPEED_DECEL: float = 1600.0
const MOVE_HORIZONTAL_SPEED_ACCEL: float = 1200.0

const FIRE_INTERVAL: float = 0.09
const FIRE_Y_DIRECTION_ROTATION_SPEED: float = 4.0

const DAMAGE_RED_FLASH_TIME: float = 0.05

var vec_movement: Vector2 = Vector2(0, 0)

var jump_ready: bool = true

var active: bool = false

var facing: int = Facing.RIGHT

var fire_timer: float = 0
var fire_y_direction: float = 0
var is_firing: bool = false

var damage_red_flash_timer: float = 0


func _init():
	Global.player = self


func _physics_process(delta):
	if not active:
		hide()
		return
		
	show()
	
	_process_jump()
	_process_move(delta)
	_process_gravity(delta)
	
	move_and_slide(vec_movement, Vector2.UP, false, 4, 0.785398, false)
	
	_process_fire(delta)
	# _process_damage_red_flash(delta)


func _process_gravity(delta):
	if is_on_floor() and not Input.is_action_just_pressed("player_jump"):
		vec_movement.y = GRAVITY_CLAMP
	elif not is_on_floor() and vec_movement.y <= 0:
		vec_movement.y += GRAVITY * delta * GRAVITY_PLAYER_MOVING_UP_CO
	elif not is_on_floor() and vec_movement.y > 0:
		vec_movement.y += GRAVITY * delta * GRAVITY_PLAYER_MOVING_DOWN_CO
		
	# y < 0, moving up, y > 0, moving down
	

func _process_jump():
	if Input.is_action_just_pressed("player_jump") and not Input.is_action_pressed("player_aim_down") and jump_ready:
		vec_movement.y = -JUMP_SPEED
		jump_ready = false
		
		$AnimatedSprite.frame = 0
		$AnimatedSprite.play("jump")
		
		$ASPJump.play()
	elif Input.is_action_just_pressed("player_jump") and Input.is_action_pressed("player_aim_down") and jump_ready:
		if get_slide_collision(0).collider.collision_layer == 2048:
			get_slide_collision(0).collider.call("disable")
		
	elif is_on_floor() and not jump_ready:
		jump_ready = true


func _process_move(delta):
	if Input.is_action_pressed("player_move_left"):
		facing = Facing.LEFT
		$AnimatedSprite.flip_h = true
		if $AnimatedSprite.animation != "jump":
			$AnimatedSprite.play("run")
		if vec_movement.x > -MOVE_HORIZONTAL_SPEED_MAX:
			if vec_movement.x > 0:
				vec_movement.x -= MOVE_HORIZONTAL_SPEED_DECEL * delta
			else:
				vec_movement.x -= MOVE_HORIZONTAL_SPEED_ACCEL * delta
	elif Input.is_action_pressed("player_move_right"):
		facing = Facing.RIGHT
		$AnimatedSprite.flip_h = false
		if $AnimatedSprite.animation != "jump":
			$AnimatedSprite.play("run")
		if vec_movement.x < MOVE_HORIZONTAL_SPEED_MAX:
			if vec_movement.x < 0:
				vec_movement.x += MOVE_HORIZONTAL_SPEED_DECEL * delta
			else:
				vec_movement.x += MOVE_HORIZONTAL_SPEED_ACCEL * delta
	else:
		if vec_movement.x > 0:
			vec_movement.x -= MOVE_HORIZONTAL_SPEED_DECEL * delta
			if vec_movement.x < 0:
				vec_movement.x = 0
		elif vec_movement.x < 0:
			vec_movement.x += MOVE_HORIZONTAL_SPEED_DECEL * delta
			if vec_movement.x > 0:
				vec_movement.x = 0
		if $AnimatedSprite.animation != "jump":
			$AnimatedSprite.play("default")
	
	
func _process_fire(delta):
	if Input.is_action_just_pressed("player_weapon_fire"):
		is_firing = not is_firing
		if not is_firing:
			fire_timer = 0
		
	if Input.is_action_pressed("player_aim_up"):
		fire_y_direction = -1
#		if fire_y_direction > -2:
#			fire_y_direction -= FIRE_Y_DIRECTION_ROTATION_SPEED * delta
#			if fire_y_direction < -2:
#				fire_y_direction = -2
	elif Input.is_action_pressed("player_aim_down"):
		fire_y_direction = 1
#		if fire_y_direction < 2:
#			fire_y_direction += FIRE_Y_DIRECTION_ROTATION_SPEED * delta
#			if fire_y_direction > 2:
#				fire_y_direction = 2
	else:
		fire_y_direction = 0
#		if fire_y_direction < 0:
#			fire_y_direction += FIRE_Y_DIRECTION_ROTATION_SPEED * delta
#			if fire_y_direction > 0:
#				fire_y_direction = 0
#		elif fire_y_direction > 0:
#			fire_y_direction -= FIRE_Y_DIRECTION_ROTATION_SPEED * delta
#			if fire_y_direction < 0:
#				fire_y_direction = 0
				
	if is_firing:
		if fire_timer <= 0:
			var p: KinematicBody2D = Global.player_projectile_basic.instance()
			
			match facing:
				Facing.LEFT:
					if $ProjectileSpawnPos.position.x > 0:
						$ProjectileSpawnPos.position.x = -$ProjectileSpawnPos.position.x
						$ProjectileSpawnPos/Sprite.scale.x = -1
						$ProjectileSpawnPos/Sprite.position.x = -4
					p.direction = Vector2(-1, fire_y_direction)
					
#					if Input.is_action_pressed("player_aim_up"):
#						p.direction = Vector2(-1, -1)
#					elif Input.is_action_pressed("player_aim_down"):
#						p.direction = Vector2(-1, 1)
#					else:
#						p.direction = Vector2.LEFT
				Facing.RIGHT:
					if $ProjectileSpawnPos.position.x < 0:
						$ProjectileSpawnPos.position.x = -$ProjectileSpawnPos.position.x
						$ProjectileSpawnPos/Sprite.scale.x = 1
						$ProjectileSpawnPos/Sprite.position.x = 4
					p.direction = Vector2(1, fire_y_direction)
					
#					if Input.is_action_pressed("player_aim_up"):
#						p.direction = Vector2(1, -1)
#					elif Input.is_action_pressed("player_aim_down"):
#						p.direction = Vector2(1, 1)
#					else:
#						p.direction = Vector2.RIGHT
						
			p.position = $ProjectileSpawnPos.get_global_transform().get_origin()
			get_parent().add_child(p)
			
			fire_timer = FIRE_INTERVAL
			$ProjectileSpawnPos/Sprite.visible = true
		else:
			fire_timer -= delta
			$ProjectileSpawnPos/Sprite.visible = false
	else:
		$ProjectileSpawnPos/Sprite.visible = false


func _process_damage_red_flash(delta):
	if damage_red_flash_timer > 0:
		damage_red_flash_timer -= delta
		modulate = Color.red
	else:
		modulate = Color.white

		
func damage(damage: float):
	# hp -= damage
	# damage_red_flash_timer = DAMAGE_RED_FLASH_TIME

	Global.player_died = true
	$ASPDied.play()

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "jump":
		$AnimatedSprite.play("default")


func _on_VisibilityNotifier_screen_exited():
	Global.player_died = true
	$ASPDied.play()
