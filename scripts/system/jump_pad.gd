extends Area2D


var disabled = false


func _ready():
	$AnimatedSprite.play()


func _physics_process(delta):
	if Input.is_action_just_pressed("player_jump") and get_overlapping_bodies().size() > 0:
		disabled = true
		$AnimatedSprite.play("puff")


func _on_JumpPad_body_entered(body):
	if body.collision_layer == 1 and not disabled:
		body.set("jump_ready", true)


func _on_JumpPad_body_exited(body):
	if body.collision_layer == 1 and not disabled:
		body.set("jump_ready", false)


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "puff":
		$AnimatedSprite.play("default")
		disabled = false
